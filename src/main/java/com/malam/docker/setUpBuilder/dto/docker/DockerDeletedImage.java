package com.malam.docker.setUpBuilder.dto.docker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DockerDeletedImage {
    @JsonSetter("Untagged")
    private String untagged;

    @JsonSetter("Deleted")
    private String deleted;
}
