package com.malam.docker.setUpBuilder.dto.docker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

import java.util.List;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DockerPruneResponse {
    @JsonSetter("ImagesDeleted")
    private List<DockerDeletedImage> imagesDeleted;

    @JsonSetter("SpaceReclaimed")
    private Long spaceReclaimed;
}
