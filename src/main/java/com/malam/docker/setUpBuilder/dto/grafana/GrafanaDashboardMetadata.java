package com.malam.docker.setUpBuilder.dto.grafana;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Yehoshua Susswein.
 * 05 / 07 / 2021
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GrafanaDashboardMetadata {
    private String id;
    private String uid;
    private String title;
}
