package com.malam.docker.setUpBuilder.dto.kibana;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Yehoshua Susswein.
 * 05 / 07 / 2021
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class KibanaFullStatus {
    private KibanaStatus status;
}
