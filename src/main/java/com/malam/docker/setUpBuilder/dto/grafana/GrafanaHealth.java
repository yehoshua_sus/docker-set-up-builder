package com.malam.docker.setUpBuilder.dto.grafana;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Yehoshua Susswein.
 * 06 / 07 / 2021
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GrafanaHealth {
    private String commit;
    private String database;
    private String version;
}
