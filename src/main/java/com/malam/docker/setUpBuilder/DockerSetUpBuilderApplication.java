package com.malam.docker.setUpBuilder;

import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.consts.BuildMode;
import com.malam.docker.setUpBuilder.consts.OSEnum;
import com.malam.docker.setUpBuilder.service.*;
import com.malam.docker.setUpBuilder.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@Slf4j
@SpringBootApplication
public class DockerSetUpBuilderApplication implements ApplicationRunner {
	@Autowired
	private BuildService buildService;

	@Autowired
	private AppProperties appProperties;

	@Autowired
	private InfoService infoService;

	@Autowired
	private StartService startService;

	@Autowired
	private StopService stopService;

	@Autowired
	private GrafanaService grafanaService;

	public static void main(String[] args) {
		SpringApplication.run(DockerSetUpBuilderApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		if (BuildMode.INFO.equals(appProperties.getBuildMode())) {
			infoService.showInfo();
		}
		else if (BuildMode.EXPORT_GRAFANA_DASHBOARDS.equals(appProperties.getBuildMode())) {
			grafanaService.exportDashboards();
		}
		else if (BuildMode.IMPORT_GRAFANA_DASHBOARDS.equals(appProperties.getBuildMode())) {
			File exportedGrafanaFolder = OSEnum.getGrafanaDashboardsDirectory();
			if (appProperties.getExportedGrafanaFiles() != null && !appProperties.getExportedGrafanaFiles().isEmpty()) {
				exportedGrafanaFolder = new File(appProperties.getExportedGrafanaFiles());
			}

			grafanaService.importDashboards(OSEnum.getGrafanaDatasourceDirectory(), exportedGrafanaFolder);
		}
		else if (BuildMode.START_MANAGEMENT_SERVICE.equals(appProperties.getBuildMode())) {
			startService.startManagementService();
		}
		else if (BuildMode.STOP_MANAGEMENT_SERVICE.equals(appProperties.getBuildMode())) {
			stopService.stopManagementService();
		}
		else if (BuildMode.DELETE_PREV_DOCKER_SET_UP.equals(appProperties.getBuildMode())) {
			stopService.cleanBeforeBuild();

			File dockerSetUpDirectory = new File(OSEnum.getDockerSetUpPath());
			if (dockerSetUpDirectory.exists()) {
				FileUtils.deleteQuietly(dockerSetUpDirectory);
			}
		}
		else {
			buildService.build();
		}
	}
}
