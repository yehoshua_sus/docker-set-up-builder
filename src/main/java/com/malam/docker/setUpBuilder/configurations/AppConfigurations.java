package com.malam.docker.setUpBuilder.configurations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.malam.docker.setUpBuilder.consts.OSEnum;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

import java.io.File;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
@Configuration
public class AppConfigurations {
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public RestTemplate restDockerTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = restTemplateBuilder.build();

        return restTemplate;
    }

    @Bean
    public RestTemplate restKibanaTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = restTemplateBuilder.build();

        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("elastic", "Q1w2e3r4"));

        return restTemplate;
    }

    @Bean
    public RestTemplate restGrafanaTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = restTemplateBuilder.build();

        String password;
        String dockerSetUpPath = OSEnum.getDockerSetUpPath();
        if (new File(dockerSetUpPath).exists()) {
            password = "admin_password";
        }
        else {
            password = "admin";
        }

        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("admin", password));

        return restTemplate;
    }
}
