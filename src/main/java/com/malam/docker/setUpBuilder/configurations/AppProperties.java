package com.malam.docker.setUpBuilder.configurations;

import com.malam.docker.setUpBuilder.consts.BuildMode;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app")
@Data
public class AppProperties {
    private BuildMode buildMode;

    private String exportGrafanaServerIP;

    private String exportedGrafanaFiles;

    private Boolean skipBasicSetUp;

    private Boolean skipDockerUpDown;

    private String calculations;

    private String reports;

    private String halmanMiddleware;

    private String gateway;

    private String mapal;

    private String gatewayUI;

    private String mapalUI;

    private String rmqHost;

    private String mongoHost;

    private String nexusURL;

    private String network;

    private String memES;

    private String memLogstash;
}
