package com.malam.docker.setUpBuilder.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
@Slf4j
public class OSUtils {
    public static void runCommand(String cmd) {
        try {
            if (OSUtils.isWindows()) {
                Process p = Runtime.getRuntime().exec("cmd /c start /wait \"\" " + cmd);

                p.waitFor();
            }
            else {
                ProcessBuilder pb = new ProcessBuilder(cmd);
                Process p = pb.start();
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    log.info(line);
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isWindows() {
        String OS = System.getProperty("os.name").toLowerCase();

        return OS.contains("win");
    }

    public static boolean isMac() {
        String OS = System.getProperty("os.name").toLowerCase();

        return OS.contains("mac");
    }

    public static boolean isUnix() {
        String OS = System.getProperty("os.name").toLowerCase();

        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }

    public static String getOS() {
        if (isWindows()) {
            return "win";
        } else if (isMac()) {
            return "osx";
        } else if (isUnix()) {
            return "uni";
        } else {
            return null;
        }
    }
}
