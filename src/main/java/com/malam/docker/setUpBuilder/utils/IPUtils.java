package com.malam.docker.setUpBuilder.utils;

import java.net.InetAddress;
import java.net.URI;
import java.net.URL;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
public class IPUtils {
    public static URI getURI(String url) {
        try {
            return new URL(url).toURI();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getIP() {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();

            String ip = inetAddress.toString();

            if (ip.contains("/")) {
                ip = ip.substring(ip.indexOf("/") + 1);
            }

            return ip;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
