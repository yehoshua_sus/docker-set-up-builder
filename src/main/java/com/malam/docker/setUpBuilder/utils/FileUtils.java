package com.malam.docker.setUpBuilder.utils;

import com.malam.docker.setUpBuilder.consts.OSEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.DefaultResourceLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
@Slf4j
public class FileUtils extends org.apache.commons.io.FileUtils {
    public static void replaceContent(File file, CharSequence target, CharSequence replacement) {
        replaceContent(file, Arrays.asList(target), Arrays.asList(replacement));
    }

    public static String getContent(File file) {
        try {
            String content = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);

            return content;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeInfoFile(Path path, String content) {
        try {
            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void replaceContent(File file, List<CharSequence> targets, List<CharSequence> replacements) {
        try {
            Path path = file.toPath();

            String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);

            for (int i = 0;i < targets.size();i++) {
                content = content.replace(targets.get(i), replacements.get(i));
            }

            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void makeSHFilesExecutable(File f) {
        if (f.isFile()) {
            String filePath = f.getAbsolutePath();

            if (filePath.endsWith(".sh")) {
                try {
                    Runtime.getRuntime().exec("chmod u+x " + filePath);
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        } else {
            for (File subFile : f.listFiles()) {
                makeSHFilesExecutable(subFile);
            }
        }
    }

    public static void removeFilesByExtension(File f, String extension) {
        if (f.isFile()) {
            String filePath = f.getAbsolutePath();

            if (filePath.endsWith("." + extension)) {
                FileUtils.deleteQuietly(f);
            }
        } else {
            for (File subFile : f.listFiles()) {
                removeFilesByExtension(subFile, extension);
            }
        }
    }

    public static List<File> findMatchingFiles(File f, String name) {
        List<File> foundFiles = new ArrayList<>();

        if (f.isFile()) {
            String fileName = f.getName();

            if (fileName.equals(name)) {
                foundFiles.add(f);
            }
            return foundFiles;
        } else {
            for (File subFile : f.listFiles()) {
                foundFiles.addAll(findMatchingFiles(subFile, name));
            }
        }

        return foundFiles;
    }

    public static void copyFromJar(final String sourcePath, final Path target) {
        try {

            URL url = FileUtils.class.getClassLoader().getResource(sourcePath);

            String protocol = url.getProtocol();
            log.info("protocol: " + protocol);

            if ("jar".equals(url.getProtocol())) {
                String path = url.toURI().toString();

                String[] pathParts = path.split("!");
                String pathToJarFile = pathParts[0].replace("jar:file:/", "").replace("/", File.separator);
                if (!OSUtils.isWindows()) {
                    pathToJarFile = "/" + pathToJarFile;
                }

                String resourcePrefix = pathParts[1].substring(1) + pathParts[2];

                java.util.jar.JarFile jar = new java.util.jar.JarFile(pathToJarFile);
                java.util.Enumeration enumEntries = jar.entries();
                while (enumEntries.hasMoreElements()) {
                    java.util.jar.JarEntry jarEntry = (java.util.jar.JarEntry) enumEntries.nextElement();

                    if (jarEntry.getName().startsWith(resourcePrefix)) {
                        File fileInFS = new File(jarEntry.getName().replace(resourcePrefix, OSEnum.getDockerSetUpPath()));

                        if (jarEntry.isDirectory()) {
                            log.debug("creating folder: " + fileInFS.getPath());
                            fileInFS.mkdir();
                        }
                        else {
                            log.debug("creating file: " + fileInFS.getPath());

                            InputStream input = jar.getInputStream(jarEntry);

                            FileUtils.copyInputStreamToFile(input, fileInFS);
                        }
                    }
                }
                jar.close();
            }
            else {
                URI uri  = new DefaultResourceLoader().getResource("classpath:" + sourcePath).getURI();

                Path jarPath;

                jarPath = Paths.get(uri);

                Files.walkFileTree(jarPath, new SimpleFileVisitor<Path>() {

                    private Path currentTarget;

                    @Override
                    public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                        currentTarget = target.resolve(jarPath.relativize(dir)
                                .toString());
                        Files.createDirectories(currentTarget);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                        Files.copy(file, target.resolve(jarPath.relativize(file)
                                .toString()), StandardCopyOption.REPLACE_EXISTING);
                        return FileVisitResult.CONTINUE;
                    }

                });
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
