package com.malam.docker.setUpBuilder.service;

import com.malam.docker.setUpBuilder.dto.docker.DockerPruneResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static com.malam.docker.setUpBuilder.utils.IPUtils.getURI;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
@Service
public class DockerServiceImpl implements DockerService {
    @Autowired
    @Qualifier("restDockerTemplate")
    private RestTemplate restDockerTemplate;

    @Override
    public DockerPruneResponse pruneImages() {
        try {
            String filtersStr = "{\"dangling\": [\"false\"]}";

            String filters = URLEncoder.encode(filtersStr, StandardCharsets.UTF_8.toString());

            String url = "http://localhost:2375/v1.41" + "/images/prune?filters=" + filters;

            RequestEntity<Void> requestEntity = RequestEntity.post(getURI(url)).
                    accept(MediaType.APPLICATION_JSON).build();
            ResponseEntity<DockerPruneResponse> response = restDockerTemplate.exchange(requestEntity, DockerPruneResponse.class);

            return response.getBody();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
