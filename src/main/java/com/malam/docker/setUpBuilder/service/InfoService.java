package com.malam.docker.setUpBuilder.service;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
public interface InfoService {
    void showInfo();
}
