package com.malam.docker.setUpBuilder.service;

import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.dto.grafana.GrafanaDashboardMetadata;
import com.malam.docker.setUpBuilder.dto.grafana.GrafanaHealth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.malam.docker.setUpBuilder.utils.FileUtils.getContent;
import static com.malam.docker.setUpBuilder.utils.FileUtils.writeInfoFile;
import static com.malam.docker.setUpBuilder.utils.IPUtils.getURI;

/**
 * Created by Yehoshua Susswein.
 * 06 / 07 / 2021
 */
@Slf4j
@Service
public class GrafanaServiceImpl implements GrafanaService {
    @Autowired
    private AppProperties appProperties;

    @Autowired
    @Qualifier("restGrafanaTemplate")
    private RestTemplate restGrafanaTemplate;

    @Override
    public boolean grafanaIsUp() {
        int numOfTries = 0;

        while (numOfTries < 50) {
            try {
                GrafanaHealth grafanaHealth = restGrafanaTemplate.getForObject("http://localhost/grafana/api/health", GrafanaHealth.class);

                if (grafanaHealth != null) {
                    if ("ok".equals(grafanaHealth.getDatabase())) {
                        return true;
                    }
                }
            }
            catch (Exception e) {
            }

            numOfTries++;

            log.info("looks like after " + numOfTries + " tries grafana is still not up, will check again in another 5 sec");

            try {
                Thread.sleep(5000l);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        log.warn("looks like there is a problem with grafana");

        return false;
    }

    @Override
    public void exportDashboards() {
        ResponseEntity<GrafanaDashboardMetadata[]> response =
                restGrafanaTemplate.getForEntity("http://" + appProperties.getExportGrafanaServerIP() + "/grafana/api/search?folderIds=0", GrafanaDashboardMetadata[].class);

        for (GrafanaDashboardMetadata dashboardMetadata : response.getBody()) {
            log.info("exporting dashboard: " + dashboardMetadata.getTitle());

            String url = "http://" + appProperties.getExportGrafanaServerIP() + "/grafana/api/dashboards/uid/" + dashboardMetadata.getUid();

            ResponseEntity<String> contentResponseEntity =
                    restGrafanaTemplate.getForEntity(url, String.class);

            String content = contentResponseEntity.getBody();

            content = content.replace("\"id\":" + dashboardMetadata.getId() + ",", "\"id\":null,");

            String fileName = dashboardMetadata.getTitle().replace(" ", "_") + ".json";

            Path path = Paths.get(appProperties.getExportedGrafanaFiles() + File.separator + fileName);

            writeInfoFile(path, content);
        }
    }

    @Override
    public void importDashboards(File exportedGrafanaDataSourceFolder, File exportedGrafanaDashboardFolder) {
        for (File file : exportedGrafanaDataSourceFolder.listFiles()) {
            log.info("importing datasource file: " + file.getName());

            String content = getContent(file);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            RequestEntity<String> requestEntity = RequestEntity.post(getURI("http://localhost/grafana/api/datasources")).
                    accept(MediaType.APPLICATION_JSON).
                    contentType(MediaType.APPLICATION_JSON).body(content);
            ResponseEntity<String> response = restGrafanaTemplate.exchange(requestEntity, String.class);

            log.info(response.getBody());
        }

        for (File file : exportedGrafanaDashboardFolder.listFiles()) {
            log.info("importing dashboard file: " + file.getName());

            String content = getContent(file);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            RequestEntity<String> requestEntity = RequestEntity.post(getURI("http://localhost/grafana/api/dashboards/db")).
                    accept(MediaType.APPLICATION_JSON).
                    contentType(MediaType.APPLICATION_JSON).body(content);
            ResponseEntity<String> response = restGrafanaTemplate.exchange(requestEntity, String.class);

            log.info(response.getBody());
        }
    }
}
