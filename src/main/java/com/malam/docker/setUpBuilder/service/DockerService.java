package com.malam.docker.setUpBuilder.service;

import com.malam.docker.setUpBuilder.dto.docker.DockerPruneResponse;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
public interface DockerService {
    DockerPruneResponse pruneImages();
}
