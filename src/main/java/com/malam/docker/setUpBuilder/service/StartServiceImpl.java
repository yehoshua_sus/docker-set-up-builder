package com.malam.docker.setUpBuilder.service;

import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.consts.OSEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.malam.docker.setUpBuilder.utils.OSUtils.runCommand;

/**
 * Created by Yehoshua Susswein.
 * 05 / 07 / 2021
 */
@Slf4j
@Service
public class StartServiceImpl implements StartService {
    @Autowired
    private AppProperties appProperties;

    @Override
    public void startManagementService() {
        log.info("running BuildManagmentServer script");
        runCommand(OSEnum.getBuildManagmentServerFile(appProperties.getNetwork()));

        log.info("running BuildNginx script");
        runCommand(OSEnum.getBuildNginxFile(appProperties.getNetwork()));
    }
}
