package com.malam.docker.setUpBuilder.service;

/**
 * Created by Yehoshua Susswein.
 * 05 / 07 / 2021
 */
public interface StopService {
    void stopManagementService();

    void cleanBeforeBuild();
}
