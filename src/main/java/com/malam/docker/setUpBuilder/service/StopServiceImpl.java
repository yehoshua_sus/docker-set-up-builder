package com.malam.docker.setUpBuilder.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.consts.OSEnum;
import com.malam.docker.setUpBuilder.dto.docker.DockerPruneResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.malam.docker.setUpBuilder.utils.OSUtils.runCommand;

/**
 * Created by Yehoshua Susswein.
 * 05 / 07 / 2021
 */
@Slf4j
@Service
public class StopServiceImpl implements StopService {
    @Autowired
    private AppProperties appProperties;

    @Autowired
    private DockerService dockerService;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void stopManagementService() {
        log.info("running DeleteNginx script");
        runCommand(OSEnum.getDeleteNginxFile(appProperties.getNetwork()));

        log.info("running DeleteManagmentServer script");
        runCommand(OSEnum.getDeleteManagmentServerFile(appProperties.getNetwork()));
    }

    @Override
    public void cleanBeforeBuild() {
        try {
            stopManagementService();

            DockerPruneResponse pruneResponse = dockerService.pruneImages();
            log.info(objectMapper.writeValueAsString(pruneResponse));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
