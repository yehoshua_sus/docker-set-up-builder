package com.malam.docker.setUpBuilder.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.utils.IPUtils;
import com.malam.docker.setUpBuilder.utils.OSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Yehoshua Susswein.
 * 08 / 07 / 2021
 */
@Slf4j
@Service
public class InfoServiceImpl implements InfoService {
    @Autowired
    private AppProperties appProperties;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void showInfo() {
        try {
            log.info("isDebugEnabled: " + log.isDebugEnabled());
            log.info("version: 1.1");
            log.info("AppProperties: " + objectMapper.writeValueAsString(appProperties));
            log.info("OS: " + OSUtils.getOS());
            log.info("IP: " + IPUtils.getIP());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
