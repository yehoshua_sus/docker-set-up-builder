package com.malam.docker.setUpBuilder.service;

import com.malam.docker.setUpBuilder.configurations.AppProperties;
import com.malam.docker.setUpBuilder.consts.BuildMode;
import com.malam.docker.setUpBuilder.consts.OSEnum;
import com.malam.docker.setUpBuilder.dto.kibana.KibanaFullStatus;
import com.malam.docker.setUpBuilder.dto.kibana.KibanaOverallStatus;
import com.malam.docker.setUpBuilder.dto.kibana.KibanaStatus;
import com.malam.docker.setUpBuilder.utils.FileUtils;
import com.malam.docker.setUpBuilder.utils.IPUtils;
import com.malam.docker.setUpBuilder.utils.OSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.malam.docker.setUpBuilder.consts.OSEnum.*;
import static com.malam.docker.setUpBuilder.utils.FileUtils.*;
import static com.malam.docker.setUpBuilder.utils.OSUtils.runCommand;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
@Slf4j
@Service
public class BuildServiceImpl implements BuildService {
    @Autowired
    private AppProperties appProperties;

    @Autowired
    private InfoService infoService;

    @Autowired
    private GrafanaService grafanaService;

    @Autowired
    private StartService startService;

    @Autowired
    private StopService stopService;

    @Autowired
    @Qualifier("restKibanaTemplate")
    private RestTemplate restKibanaTemplate;

    @Override
    public void build() {
        try {
            infoService.showInfo();

            if (BuildMode.RUN.equals(appProperties.getBuildMode())) {
                String dockerSetUpPath = OSEnum.getDockerSetUpPath();

                if (Boolean.TRUE.equals(appProperties.getSkipBasicSetUp())) {
                    if (!new File(dockerSetUpPath).exists()) {
                        log.info(dockerSetUpPath + " is missing ignoring request to skip basic set up");

                        basicSetUp(dockerSetUpPath);
                    }
                    else {
                        log.info("skipping basic set up");
                    }
                }
                else {
                    basicSetUp(dockerSetUpPath);
                }

                if (!Boolean.TRUE.equals(appProperties.getSkipDockerUpDown())) {
                    log.info("starting with DockerUpDown stage");

                    startService.startManagementService();

                    if (kibanaIsUp()) {
                        log.info("running Build KibanaImporter script");
                        runCommand(OSEnum.getBuildKibanaImporterFile(appProperties.getNetwork()));

                        log.info("running Delete KibanaImporter script");
                        runCommand(OSEnum.getDeleteKibanaImporterFile(appProperties.getNetwork()));
                    }

                    try {
                        log.info("loading grafana dashboards");
                        if (grafanaService.grafanaIsUp()) {
                            grafanaService.importDashboards(OSEnum.getGrafanaDatasourceDirectory(), OSEnum.getGrafanaDashboardsDirectory());
                        }
                    }
                    catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }

                    stopService.stopManagementService();

                    if (!OSUtils.isWindows()) {
                        OSUtils.runCommand("/home/manager/malam/Docker-Set-Up/Build/allow_access_to_files.sh");
                    }

                    log.info("finished with DockerUpDown stage");
                }

                log.info("starting with fix files stage of Docker-Set-Up");

                fixDockerComposeFilesAfterDockerUpdate();
                fixRabbitMQ();
                fixLogstash();
                fixNginx();
                fixPrometheus();
                fixES();

                log.info("finished with fix files stage of Docker-Set-Up");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private boolean kibanaIsUp() {
        int numOfTries = 0;

        while (numOfTries < 50) {
            try {
                KibanaFullStatus kibanaFullStatus = restKibanaTemplate.getForObject("http://localhost/kibana/api/status", KibanaFullStatus.class);

                if (kibanaFullStatus != null) {
                    KibanaStatus kibanaStatus = kibanaFullStatus.getStatus();

                    if (kibanaStatus != null) {
                        KibanaOverallStatus overall = kibanaStatus.getOverall();

                        if (overall != null) {
                            if ("green".equals(overall.getState())) {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
            }

            numOfTries++;

            log.info("looks like after " + numOfTries + " tries kibana is still not up, will check again in another 5 sec");

            try {
                Thread.sleep(5000l);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        log.warn("looks like there is a problem with kibana");

        return false;
    }

    private void basicSetUp(String dockerSetUpPath) {
        if (new File(dockerSetUpPath).exists()) {
            throw new RuntimeException(dockerSetUpPath + " already exists, please change name of directory, so that Docker-Set-Up directory can be built");
        }
        else {
            log.info("starting with basic Docker-Set-Up");

            FileUtils.copyFromJar(DOCKER_SET_UP, Paths.get(dockerSetUpPath));

            File dockerSetUpFolder = new File(dockerSetUpPath);
            if (OSUtils.isWindows()) {
                removeFilesByExtension(dockerSetUpFolder, "sh");
            }
            else {
                makeSHFilesExecutable(dockerSetUpFolder);

                removeFilesByExtension(dockerSetUpFolder, "bat");
            }

            stopService.cleanBeforeBuild();

            List<File> removeThisFiles = FileUtils.findMatchingFiles(new File(dockerSetUpPath), "remove-this.txt");
            removeThisFiles.stream().forEach(removeFile -> removeFile.delete());

            fixBuildImagesInstructions();

            fixEnvFiles();

            fixDockerComposeFiles();

            fixApplicationYamlFile("ElasticManager", "http://localhost/", "http://" + IPUtils.getIP() + "/");

            log.debug("running set up network script");
            runCommand(getDockerSetUpNetworkBuildFile());

            log.debug("running set up volume script");
            runCommand(getDockerSetUpVolumeBuildFile());

            log.info("finished with basic Docker-Set-Up");
        }
    }

    private void fixMem(String mem, String memOriginal, String jvmOptions) {
        List<CharSequence> targets = new ArrayList<>();
        targets.add("Xms" + memOriginal);
        targets.add("Xmx" + memOriginal);

        List<CharSequence> replacements = new ArrayList<>();
        replacements.add("Xms" + mem);
        replacements.add("Xmx" + mem);

        replaceContent(new File(jvmOptions), targets, replacements);
    }

    private void fixES() {
        String memES = appProperties.getMemES();
        if (memES != null && !memES.isEmpty()) {
            String jvmOptions =
                    OSEnum.getDockerSetUpVolumePath() +
                            File.separator + "Elasticsearch" +
                            File.separator + "usr" +
                            File.separator + "share" +
                            File.separator + "elasticsearch" +
                            File.separator + "config" +
                            File.separator + "jvm.options";

            fixMem(memES, "1g", jvmOptions);
        }
    }

    private void fixPrometheus() {
        String confFilePath =
                OSEnum.getDockerSetUpVolumePath() +
                        File.separator + "Prometheus" +
                        File.separator + "etc" +
                        File.separator + "prometheus" +
                        File.separator + "prometheus.yml";

        List<CharSequence> targets = new ArrayList<>();
        targets.add("elastic_manager_external_host: 'localhost'");

        List<CharSequence> replacements = new ArrayList<>();
        replacements.add("elastic_manager_external_host: '" + IPUtils.getIP() + "'");

        String rmqHost = appProperties.getRmqHost();
        if (rmqHost != null && !rmqHost.isEmpty()) {
            targets.add("['rabbitmq:15672']");
            replacements.add("['" + rmqHost + ":15672']");
        }

        String mapal = appProperties.getMapal();
        if (mapal != null && !mapal.isEmpty()) {
            targets.add("['mapal:9081']");
            replacements.add("['" + mapal + "']");
        }

        String gateway = appProperties.getGateway();
        if (gateway != null && !gateway.isEmpty()) {
            targets.add("['halman:8080']");
            replacements.add("['" + gateway + "']");
        }

        String halmanMiddleware = appProperties.getHalmanMiddleware();
        if (halmanMiddleware != null && !halmanMiddleware.isEmpty()) {
            targets.add("['halman-middleware:8088']");
            replacements.add("['" + halmanMiddleware + "']");
        }

        String calculations = appProperties.getCalculations();
        if (calculations != null && !calculations.isEmpty()) {
            targets.add("['calculations-server:8087']");
            replacements.add("['" + calculations + "']");
        }

        String reports = appProperties.getReports();
        if (reports != null && !reports.isEmpty()) {
            targets.add("['localhost:8001']");
            replacements.add("['" + reports + "']");
        }

        replaceContent(new File(confFilePath), targets, replacements);
    }

    private void fixRabbitMQ() {
        String dockerCompose =
                OSEnum.getBuildContainerDirectory() + File.separator + appProperties.getNetwork() +
                        File.separator + "basic-rmq" +
                        File.separator + "docker-compose.yml";

        File dockerComposeAfterFirstLoadFile = new File(OSEnum.getBuildContainerDirectory() + File.separator + appProperties.getNetwork() +
                File.separator + "basic-rmq" +
                File.separator + "docker-compose-after-first-load.yml");

        String content = FileUtils.getContent(dockerComposeAfterFirstLoadFile);

        FileUtils.writeInfoFile(new File(dockerCompose).toPath(), content);

        FileUtils.deleteQuietly(dockerComposeAfterFirstLoadFile);
    }

    private void fixLogstash() {
        String rmqHost = appProperties.getRmqHost();
        if (rmqHost != null && !rmqHost.isEmpty()) {
            String confFilePath =
                    OSEnum.getDockerSetUpVolumePath() +
                            File.separator + "Logstash" +
                            File.separator + "usr" +
                            File.separator + "share" +
                            File.separator + "logstash" +
                            File.separator + "config" +
                            File.separator + "logstash.conf";

            replaceContent(new File(confFilePath), "host => \"rabbitmq\"", "host => \"" + rmqHost + "\"");
        }

        String mem = appProperties.getMemLogstash();
        if (mem != null && !mem.isEmpty()) {
            String jvmOptions =
                    OSEnum.getDockerSetUpVolumePath() +
                            File.separator + "Logstash" +
                            File.separator + "usr" +
                            File.separator + "share" +
                            File.separator + "logstash" +
                            File.separator + "config" +
                            File.separator + "jvm.options";

            fixMem(mem, "256m", jvmOptions);
        }
    }

    private void fixNginx() {
        String rmqHost = appProperties.getRmqHost();
        if (rmqHost != null && !rmqHost.isEmpty()) {
            String confFilePath = getNginxIndexPath();

            replaceContent(new File(confFilePath), "href=\"rmq\"", "href=\"" + rmqHost + "\"");
        }

        String gatewayUI = appProperties.getGatewayUI();
        if (gatewayUI != null && !gatewayUI.isEmpty()) {
            String confFilePath = getNginxIndexPath();

            replaceContent(new File(confFilePath), "href=\"http://localhost:3002\"", "href=\"" + gatewayUI + "\"");
        }

        String mapalUI = appProperties.getMapalUI();
        if (mapalUI != null && !mapalUI.isEmpty()) {
            String confFilePath = getNginxIndexPath();

            replaceContent(new File(confFilePath), "href=\"http://localhost:3003\"", "href=\"" + mapalUI + "\"");
        }
    }

    private String getNginxIndexPath() {
        return OSEnum.getDockerSetUpVolumePath() +
                File.separator + "Nginx" +
                File.separator + "usr" +
                File.separator + "share" +
                File.separator + "nginx" +
                File.separator + "html" +
                File.separator + "index.html";
    }

    private void fixApplicationYamlFile(String appName, String target, String replacement) {
        String yamlFilePath = OSEnum.getDockerSetUpVolumePath() + File.separator + appName + File.separator + "Malam" +  File.separator + appName + File.separator + "application.yaml";

        replaceContent(new File(yamlFilePath), target, replacement);
    }

    private void fixBuildImagesInstructions() {
        try {
            if (!OSUtils.isWindows()) {
                File buildContainerDirectory = OSEnum.getBuildImagesInstructionsFile();

                List<CharSequence> targets = new ArrayList<>();
                targets.add(OSEnum.WIN.getStartPath());
                targets.add("\\");
                targets.add("D:");

                List<CharSequence> replacements = new ArrayList<>();
                replacements.add(LINUX.getStartPath());
                replacements.add(File.separator);
                replacements.add("---");

                replaceContent(buildContainerDirectory, targets, replacements);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void fixEnvFiles() {
        try {
            File buildContainerDirectory = OSEnum.getBuildContainerDirectory();

            List<File> envFiles = FileUtils.findMatchingFiles(buildContainerDirectory, ".env");

            for (File envFile : envFiles) {
                List<CharSequence> targets = Arrays.asList("${Docker-Set-Up-Volume}", "${PROMETHEUS_HOST}", "${NEXUS_URL}");
                List<CharSequence> replacements = Arrays.asList(OSEnum.getDockerSetUpVolumePath() + File.separator, IPUtils.getIP(), appProperties.getNexusURL());

                replaceContent(envFile, targets, replacements);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void fixDockerComposeFiles() {
        try {
            File buildContainerDirectory = OSEnum.getBuildContainerDirectory();

            List<File> dockerComposeFiles = FileUtils.findMatchingFiles(buildContainerDirectory, "docker-compose.yml");

            for (File dockerCompose : dockerComposeFiles) {
                replaceContent(dockerCompose, "\\", File.separator);
            }

            List<File> dockerComposeAfterFirstLoadFiles = FileUtils.findMatchingFiles(buildContainerDirectory, "docker-compose-after-first-load.yml");

            for (File dockerCompose : dockerComposeAfterFirstLoadFiles) {
                replaceContent(dockerCompose, "\\", File.separator);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void fixDockerComposeFilesAfterDockerUpdate() {
        try {
            String mongoHost = appProperties.getMongoHost();
            if (mongoHost != null && !mongoHost.isEmpty()) {
                File buildContainerDirectory = OSEnum.getBuildContainerDirectory();

                List<File> dockerComposeFiles = FileUtils.findMatchingFiles(buildContainerDirectory, "docker-compose.yml");

                for (File dockerCompose : dockerComposeFiles) {
                    replaceContent(dockerCompose, "mongodb://mongo", "mongodb://" + mongoHost);
                }

                List<File> dockerComposeAfterFirstLoadFiles = FileUtils.findMatchingFiles(buildContainerDirectory, "docker-compose-after-first-load.yml");

                for (File dockerCompose : dockerComposeAfterFirstLoadFiles) {
                    replaceContent(dockerCompose, "mongodb://mongo", "mongodb://" + mongoHost);
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
