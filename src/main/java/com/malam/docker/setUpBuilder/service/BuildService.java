package com.malam.docker.setUpBuilder.service;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
public interface BuildService {
    void build();
}
