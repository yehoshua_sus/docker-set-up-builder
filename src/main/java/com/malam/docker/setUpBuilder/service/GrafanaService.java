package com.malam.docker.setUpBuilder.service;

import java.io.File;

/**
 * Created by Yehoshua Susswein.
 * 06 / 07 / 2021
 */
public interface GrafanaService {
    boolean grafanaIsUp();

    void exportDashboards();

    void importDashboards(File exportedGrafanaDataSourceFolder, File exportedGrafanaFolder);
}
