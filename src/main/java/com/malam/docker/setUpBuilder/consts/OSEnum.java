package com.malam.docker.setUpBuilder.consts;

import com.malam.docker.setUpBuilder.utils.OSUtils;

import java.io.File;

/**
 * Created by Yehoshua Susswein.
 * 30 / 06 / 2021
 */
public enum  OSEnum {
    WIN("D:\\"),
    LINUX("/home/manager/malam/");

    public static final String DOCKER_SET_UP = "Docker-Set-Up";

    private String startPath;

    OSEnum(String startPath) {
        this.startPath = startPath;
    }

    public String getStartPath() {
        return startPath;
    }

    public static String getDockerSetUpPath() {
        String dockerSetUpPath;
        if (OSUtils.isWindows()) {
            dockerSetUpPath = OSEnum.WIN.getStartPath() + DOCKER_SET_UP;
        }
        else {
            dockerSetUpPath = OSEnum.LINUX.getStartPath() + DOCKER_SET_UP;
        }

        return dockerSetUpPath;
    }

    public static String getDockerSetUpVolumePath() {
        return getDockerSetUpPath() + File.separator + "volume";
    }

    public static String getDockerSetUpNetworkBuildFile() {
        String dockerSetUpNetworkBuildFile = getDockerSetUpPath() + File.separator + "Build" + File.separator + "Network" + File.separator;
        if (OSUtils.isWindows()) {
            dockerSetUpNetworkBuildFile += "build.bat";
        }
        else {
            dockerSetUpNetworkBuildFile += "build.sh";
        }

        return dockerSetUpNetworkBuildFile;
    }

    public static String getDockerSetUpVolumeBuildFile() {
        String dockerSetUpNetworkBuildFile = getDockerSetUpPath() + File.separator + "Build" + File.separator + "Volume" + File.separator;
        if (OSUtils.isWindows()) {
            dockerSetUpNetworkBuildFile += "build.bat";
        }
        else {
            dockerSetUpNetworkBuildFile += "build.sh";
        }

        return dockerSetUpNetworkBuildFile;
    }

    public static File getBuildContainerDirectory() {
        return new File(getDockerSetUpPath() + File.separator + "Build" + File.separator + "Container");
    }

    public static File getBuildImagesInstructionsFile() {
        return new File(getDockerSetUpPath() + File.separator + "instructions"  + File.separator + "build_images.txt");
    }

    public static File getGrafanaDatasourceDirectory() {
        return new File(getDockerSetUpPath() + File.separator + "grafana"  + File.separator + "datasources");
    }

    public static File getGrafanaDashboardsDirectory() {
        return new File(getDockerSetUpPath() + File.separator + "grafana"  + File.separator + "dashboards");
    }

    public static String getBuildManagmentServerFile(String network) {
        String buildManagmentServerFile = OSEnum.getBuildContainerDirectory() + File.separator + network + File.separator + "build-managment-server";

        if (OSUtils.isWindows()) {
            buildManagmentServerFile += ".bat";
        }
        else {
            buildManagmentServerFile += ".sh";
        }

        return buildManagmentServerFile;
    }

    public static String getDeleteManagmentServerFile(String network) {
        String buildManagmentServerFile = OSEnum.getBuildContainerDirectory() + File.separator + network + File.separator + "delete-managment-server";

        if (OSUtils.isWindows()) {
            buildManagmentServerFile += ".bat";
        }
        else {
            buildManagmentServerFile += ".sh";
        }

        return buildManagmentServerFile;
    }

    public static String getBuildKibanaImporterFile(String network) {
        String buildFile = OSEnum.getBuildContainerDirectory() + File.separator + network +
                File.separator + "manager-server-kibana-importer" + File.separator + "built";

        if (OSUtils.isWindows()) {
            buildFile += ".bat";
        }
        else {
            buildFile += ".sh";
        }

        return buildFile;
    }

    public static String getDeleteKibanaImporterFile(String network) {
        String deleteFile = OSEnum.getBuildContainerDirectory() + File.separator + network +
                File.separator + "manager-server-kibana-importer" + File.separator + "delete";

        if (OSUtils.isWindows()) {
            deleteFile += ".bat";
        }
        else {
            deleteFile += ".sh";
        }

        return deleteFile;
    }

    public static String getBuildNginxFile(String network) {
        String buildFile = OSEnum.getBuildContainerDirectory() + File.separator + network +
                File.separator + "manager-server-nginx" + File.separator + "built";

        if (OSUtils.isWindows()) {
            buildFile += ".bat";
        }
        else {
            buildFile += ".sh";
        }

        return buildFile;
    }

    public static String getDeleteNginxFile(String network) {
        String deleteFile = OSEnum.getBuildContainerDirectory() + File.separator + network +
                File.separator + "manager-server-nginx" + File.separator + "delete";

        if (OSUtils.isWindows()) {
            deleteFile += ".bat";
        }
        else {
            deleteFile += ".sh";
        }

        return deleteFile;
    }
}
