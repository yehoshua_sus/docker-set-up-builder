package com.malam.docker.setUpBuilder.consts;

/**
 * Created by Yehoshua Susswein.
 * 01 / 07 / 2021
 */
public enum BuildMode {
    INFO,
    RUN,
    START_MANAGEMENT_SERVICE,
    STOP_MANAGEMENT_SERVICE,
    DELETE_PREV_DOCKER_SET_UP,
    EXPORT_GRAFANA_DASHBOARDS,
    IMPORT_GRAFANA_DASHBOARDS
}
