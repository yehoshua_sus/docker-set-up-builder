sudo chown -R manager:manager /home/manager/malam/Docker-Set-Up
sudo chmod -R 777 /home/manager/malam/Docker-Set-Up

sudo chmod go-w /home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat/filebeat.yml
sudo chmod -R go-w /home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat/module/elasticsearch
sudo chmod -R go-w /home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat/module/logstash
sudo chmod -R go-w /home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat/modules.d
sudo chmod -R go-w /home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat/modules.d.malam