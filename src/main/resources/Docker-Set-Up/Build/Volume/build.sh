docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/MongoDB/data/db --opt type=none --opt o=bind mongo_data
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/MongoDB/etc --opt type=none --opt o=bind mongo_etc
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/MongoDB/var/log/mongodb --opt type=none --opt o=bind mongo_log

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Nginx/usr/share/nginx/html --opt type=none --opt o=bind nginx_data
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Nginx/etc/nginx/conf.d --opt type=none --opt o=bind nginx_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/RabbitMQ/var/lib/rabbitmq/mnesia --opt type=none --opt o=bind rabbitmq_data
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/RabbitMQ/etc/rabbitmq --opt type=none --opt o=bind rabbitmq_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Halman/Malam/Halman/logs --opt type=none --opt o=bind halman_logs

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/HalmanMiddleware/Malam/HalmanMiddleware/logs --opt type=none --opt o=bind halman_middleware_logs
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/HalmanMiddleware/Malam/HalmanMiddleware/documents --opt type=none --opt o=bind halman_middleware_documents

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Rakefet/Malam/Rakefet/logs --opt type=none --opt o=bind rakefet_logs

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Poalim/Malam/Poalim/logs --opt type=none --opt o=bind poalim_logs

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/RakefetMock/Malam/RakefetMock/mock_response_examples --opt type=none --opt o=bind rakefet_mock_response_examples

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/SpringBootAdmin/opt/spring-boot-admin-docker --opt type=none --opt o=bind sba_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Prometheus/etc/prometheus --opt type=none --opt o=bind prometheus_conf
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Prometheus/prometheus --opt type=none --opt o=bind prometheus_data

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Alertmanager/etc/alertmanager --opt type=none --opt o=bind alertmanager_conf
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Alertmanager/alertmanager --opt type=none --opt o=bind alertmanager_data

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Elasticsearch/usr/share/elasticsearch/data --opt type=none --opt o=bind elasticsearch_data
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Elasticsearch/usr/share/elasticsearch/config --opt type=none --opt o=bind elasticsearch_config
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Elasticsearch/usr/share/elasticsearch/logs --opt type=none --opt o=bind elasticsearch_logs

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Kibana/usr/share/kibana/config --opt type=none --opt o=bind kibana_config

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Logstash/usr/share/logstash/config --opt type=none --opt o=bind logstash_config
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Logstash/usr/share/logstash/logs --opt type=none --opt o=bind logstash_logs
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Logstash/usr/share/logstash/data --opt type=none --opt o=bind logstash_data
docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Logstash/usr/share/logstash/pipeline --opt type=none --opt o=bind logstash_pipeline

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/ElasticManager/Malam/ElasticManager/logs --opt type=none --opt o=bind elasticmanager_logs

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Filebeat/usr/share/filebeat --opt type=none --opt o=bind filebeat_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/FilebeatApp/usr/share/filebeat --opt type=none --opt o=bind filebeat_app_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Grafana/var/lib/grafana --opt type=none --opt o=bind grafana_data

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/Grafana/usr/share/grafana/conf --opt type=none --opt o=bind grafana_conf

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/HalmanGatewayClient/usr/local/tomcat/webapps --opt type=none --opt o=bind halman_gateway_client_webapps

docker volume create --driver local --opt device=/home/manager/malam/Docker-Set-Up/volume/PoalimGatewayClient/usr/local/tomcat/webapps --opt type=none --opt o=bind poalim_gateway_client_webapps